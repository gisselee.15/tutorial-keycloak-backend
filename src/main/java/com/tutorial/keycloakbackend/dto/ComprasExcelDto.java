package com.tutorial.keycloakbackend.dto;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.DocumentFamily;
import com.artofsolving.jodconverter.DocumentFormat;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
import com.spire.pdf.PdfConvertOptions;
import com.spire.pdf.actions.PdfAction;
import com.spire.xls.FileFormat;
import com.spire.xls.Workbook;
import com.tutorial.keycloakbackend.models.entity.Compra;
import com.tutorial.keycloakbackend.models.entity.Factura;

public class ComprasExcelDto {

	public XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private List<Compra> listAportes;
	private OpenOfficeConnection connection;
	private Double total;

	public ComprasExcelDto(List<Compra> list, Double total) {
		this.listAportes = list;
		workbook = new XSSFWorkbook();
		this.total=total;
	}

	private void writeHeaderLine() {
		sheet = workbook.createSheet("Compras");
		
		Row row = sheet.createRow(0);

		CellStyle style = workbook.createCellStyle();
		
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		font.setFontHeight(16);
		style.setFont(font);

		createCell(row, 0, "Nro. Compra", style);
		createCell(row, 1, "Proveedor Id", style);
		createCell(row, 2, "Proveedor", style);
		createCell(row, 3, "Proveedor Encargado", style);
		createCell(row, 4, "Fecha Operación", style);
		createCell(row, 5, "Descripcion", style);
		createCell(row, 6, "Observacion", style);
		createCell(row, 7, "Monto Total", style);

	}

	private void createCell(Row row, int columnCount, Object value, CellStyle style) {
		sheet.autoSizeColumn(columnCount);
		Cell cell = row.createCell(columnCount);
		if (value instanceof Integer) {
			cell.setCellValue((Integer) value);
		} else if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		} else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		} else if (value instanceof Long) {
			cell.setCellValue((Long) value);
		} else if (value instanceof String){
			cell.setCellValue((String) value);
		}else {
			cell.setCellValue("");
		}
		cell.setCellStyle(style);
	}

	private void writeDataLines() {
		int rowCount = 1;
		XSSFFont font = workbook.createFont();
		font.setFontHeight(14);
				
		CellStyle style = workbook.createCellStyle();		
		style.setFont(font);
		
		//creando estilo de celda para daar formato numerico
		DataFormat format = workbook.createDataFormat();
		CellStyle styleNum = workbook.createCellStyle();
		styleNum = workbook.createCellStyle();
		//obs. quizas aqui se pueda cambiar a "." o "," como separadorde miles
		styleNum.setDataFormat(format.getFormat("#,#"));
		styleNum.setFont(font);
		int cont=1;
		for (Compra item : listAportes) {
			Row row = sheet.createRow(rowCount++);
			int columnCount = 0;
			if(cont!=listAportes.size()) {
			createCell(row, columnCount++, item.getId(), style);
			createCell(row, columnCount++, item.getProveedor().getId(), style);
			createCell(row, columnCount++, item.getProveedor().getNombre(), style);
			createCell(row, columnCount++, item.getProveedor().getEncargado(), style);
			createCell(row, columnCount++, item.getCreateAt().toString(), style);
			createCell(row, columnCount++, item.getDescripcion(), style);
			createCell(row, columnCount++, item.getObservacion(), style);
			createCell(row, columnCount++, item.getTotal().toString(), styleNum);	
			}else {
				 columnCount = 6;
				 createCell(row, columnCount++, "Monto Total: ", style);
				 createCell(row, columnCount++, this.total.toString(), styleNum);
			}
			cont++;

		}
	}

	public void export(HttpServletResponse response) throws IOException {
		writeHeaderLine();
		writeDataLines();

		ServletOutputStream outputStream = response.getOutputStream();
		workbook.write(outputStream);
		workbook.close();

		outputStream.close();

	}
	
	public void convertExcelToPDF(OutputStream out) throws Exception {
		writeHeaderLine();
        writeDataLines();
        openOpenOfficeConnection();
		   DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
		   DocumentFormat inputDocumentFormat = new DocumentFormat("Microsoft Excel", DocumentFamily.SPREADSHEET, "application/vnd.ms-excel", "xls");
		   inputDocumentFormat.setExportFilter(DocumentFamily.SPREADSHEET, "MS Excel 97");
		   DocumentFormat outputDocumentFormat = new DocumentFormat("Portable Document Format", DocumentFamily.TEXT,"application/pdf", "pdf");
		   outputDocumentFormat.setExportFilter(DocumentFamily.SPREADSHEET, "calc_pdf_Export");
		   
		    converter.convert(new ByteArrayInputStream(generarBytesExcel(workbook)), inputDocumentFormat, out, outputDocumentFormat);
		    openOpenOfficeConnection();
	}
	
	public void convertExcelToPDFjjj(OutputStream out) throws Exception {
		//Load the input Excel file
        Workbook workbook = new Workbook();
       

        //Fit to page
        workbook.getConverterSetting().setSheetFitToPage(true);

        //Save as PDF document
        workbook.saveToFile("ExcelToPDF.pdf",FileFormat.PDF);  
        
        
	}
	public byte[] generarBytesExcel(XSSFWorkbook hssfWorkbook) {
		   ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		   try {
		      hssfWorkbook.write(outStream);
		      outStream.close();
		   } catch (IOException e) {
		      e.printStackTrace();
		   }
		   byte[] var= outStream.toByteArray();
		   return var;
		}
	public void openOpenOfficeConnection() {
		   try {
		   if (connection == null || !connection.isConnected()) 
		         connection = new SocketOpenOfficeConnection("localhost", 8100);
		      connection.connect();
		      } catch (ConnectException e) {
		      e.printStackTrace();
		      }
		}
		public void closeOpenOfficeConnection() {
		   try {
		   if (connection != null && connection.isConnected())
		      connection.disconnect();
		   } catch (Exception e) {
		      e.printStackTrace();
		   }
		}
}
