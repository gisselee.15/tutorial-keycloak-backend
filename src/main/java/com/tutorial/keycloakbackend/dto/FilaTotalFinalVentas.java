package com.tutorial.keycloakbackend.dto;

import java.time.LocalDateTime;

import com.tutorial.keycloakbackend.models.entity.Cliente;
import com.tutorial.keycloakbackend.models.entity.Producto;

public class FilaTotalFinalVentas {
	
	
	private String id;

	private String descripcion;

	private String observacion;
	private String createAt;
	private Cliente cliente;
	private Double total;
	private String usuario;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCreateAt() {
		return createAt;
	}
	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getObservacion() {
		return observacion;
	}
	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	
}
