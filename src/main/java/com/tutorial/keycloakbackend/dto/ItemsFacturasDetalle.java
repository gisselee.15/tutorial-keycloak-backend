package com.tutorial.keycloakbackend.dto;

import com.tutorial.keycloakbackend.models.entity.Producto;

public class ItemsFacturasDetalle {
	private Long id;
	private Integer cantidad;
	private Producto producto;
	private String estado;
	private Long facturaId;
	private String nombre;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Integer getCantidad() {
		return cantidad;
	}
	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public Long getFacturaId() {
		return facturaId;
	}
	public void setFacturaId(Long facturaId) {
		this.facturaId = facturaId;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


}
