package com.tutorial.keycloakbackend.dto;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.HSSFRegionUtil;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import com.artofsolving.jodconverter.DocumentConverter;
import com.artofsolving.jodconverter.DocumentFamily;
import com.artofsolving.jodconverter.DocumentFormat;
import com.artofsolving.jodconverter.openoffice.connection.OpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.connection.SocketOpenOfficeConnection;
import com.artofsolving.jodconverter.openoffice.converter.OpenOfficeDocumentConverter;
public class DonwloadAnualPDFReport {
	private OpenOfficeConnection connection;
	/*public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
	   // Recogemos parametros para filtrar
	   int anyInici = Integer.parseInt(request.getParameter("year"));
	   String type = (String)request.getParameter("type");
	   // Preparamos información para generar el archivo XLS
	   GregorianCalendar cal;
	   try {
	   cal = new GregorianCalendar(anyInici, 0, 1);
	   } catch (Exception e) {
	   cal = new  GregorianCalendar();
	   }
	   cal.set(cal.HOUR_OF_DAY,6); cal.set(cal.MINUTE,0);cal.set(cal.SECOND, 0);cal.set(cal.MILLISECOND, 0);
	   cal.setLenient(false);
	   Map<Long, List<HashMap<String, Object>>> infoReserves = salaReservaManager.obteInfoAnual(cal);                       
	   // Generamos el archivo XLS mediante Apache POI
	   HSSFWorkbook book = exportToExcel(cal, infoReserves, salaManager.getAll(), diaFestiuManager.getAll());
	   // Gestionamos la conexion con OpenOffice y generamos el PDF
	   OutputStream out = response.getOutputStream();
	   openOpenOfficeConnection();
	   convertExcelToPDF(book, out);
	   closeOpenOfficeConnection();
	   // Preparamos el response
	   SimpleDateFormat format = new SimpleDateFormat("dd_MM_yyyy");
	   response.setContentType("application/force-download");
	   response.setHeader("Content-Transfer-Encoding", "binary");
	   response.setHeader("Content-Disposition","attachment; filename=\"Anual_" + anyInici + "_" +format.format(cal.getTime())+".pdf" + "\";"); 
	   response.flushBuffer();
	   out.close();
	   return null;                         
	}*/
	public void openOpenOfficeConnection() {
	   try {
	   if (connection == null || !connection.isConnected()) 
	         connection = new SocketOpenOfficeConnection(8100);
	      connection.connect();
	      } catch (ConnectException e) {
	      e.printStackTrace();
	      }
	}
	public void closeOpenOfficeConnection() {
	   try {
	   if (connection != null && connection.isConnected())
	      connection.disconnect();
	   } catch (Exception e) {
	      e.printStackTrace();
	   }
	}
	private void convertExcelToPDF(HSSFWorkbook wb, OutputStream out) throws Exception {
	   DocumentConverter converter = new OpenOfficeDocumentConverter(connection);
	   DocumentFormat inputDocumentFormat = new DocumentFormat("Microsoft Excel", DocumentFamily.SPREADSHEET, "application/vnd.ms-excel", "xls");
	   inputDocumentFormat.setExportFilter(DocumentFamily.SPREADSHEET, "MS Excel 97");
	   DocumentFormat outputDocumentFormat = new DocumentFormat("Portable Document Format", DocumentFamily.TEXT,"application/pdf", "pdf");
	   outputDocumentFormat.setExportFilter(DocumentFamily.SPREADSHEET, "calc_pdf_Export");
	   converter.convert(new ByteArrayInputStream(generarBytesExcel(wb)), inputDocumentFormat, out, outputDocumentFormat);
	}
	public byte[] generarBytesExcel(HSSFWorkbook hssfWorkbook) {
	   ByteArrayOutputStream outStream = new ByteArrayOutputStream();
	   try {
	      hssfWorkbook.write(outStream);
	      outStream.close();
	   } catch (IOException e) {
	      e.printStackTrace();
	   }
	   return outStream.toByteArray();
	}
	}