package com.tutorial.keycloakbackend.controller;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tutorial.keycloakbackend.dto.ComprasExcelDto;
import com.tutorial.keycloakbackend.dto.VentasExcelDto;
import com.tutorial.keycloakbackend.models.entity.Compra;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.ItemCompra;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.entity.Proveedor;
import com.tutorial.keycloakbackend.models.service.IClienteService;
import com.tutorial.keycloakbackend.models.service.ICompraService;
import com.tutorial.keycloakbackend.models.service.ICompraService;
import com.tutorial.keycloakbackend.models.service.IProductoService;
import com.tutorial.keycloakbackend.models.service.IProveedorService;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
@ComponentScan("com.tutorial.keycloakbackend.controller")
public class CompraRestController {

	@Autowired
	private ICompraService facturaService;

	@Autowired
	private IProveedorService clienteService;
	@Autowired
	private IProductoService productoService;
	@GetMapping("/compras/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Compra show(@PathVariable Long id) {
		return clienteService.findCompraById(id);
	}
	
	@DeleteMapping("/compras/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		clienteService.deleteCompraById(id);
	}
	
	@GetMapping("/compras/filtrar-productos/{term}")
	@ResponseStatus(HttpStatus.OK)
	public List<Producto> filtrarProductos(@PathVariable String term){
		return clienteService.findProductoByNombre(term);
	}
	
	@PostMapping("/compras")
	@ResponseStatus(HttpStatus.CREATED)
	public Compra crear(@RequestBody Compra factura) {
		try {
			//agregar stok productos
			if(factura.getItems().size()>0) {
				for(ItemCompra item : factura.getItems()) {
					
					Producto clienteActual = productoService.findById(item.getProducto().getId());
			
					Producto clienteUpdated = null;
					if (clienteActual != null  && clienteActual.getTipo().equalsIgnoreCase("almacen")) {
						clienteActual.setNombre(clienteActual.getNombre());
						clienteActual.setPrecio(clienteActual.getPrecio());
						clienteActual.setCreateAt(clienteActual.getCreateAt());
						clienteActual.setTipo(clienteActual.getTipo());
						clienteActual.setExistente(clienteActual.getExistente()+item.getCantidad());
						clienteActual.setMedida(clienteActual.getMedida());
						clienteUpdated = productoService.save(clienteActual);
					}
					if (clienteActual.getTipo().equalsIgnoreCase("preparados")) {
						item.setEstado("pendiente");
					}
				}
			}
			return clienteService.saveCompra(factura);
		}catch(Exception e) {
			return null;
		}
	}
	@GetMapping("/compras")
	public List<Compra> index() {
		return facturaService.findAll();
	} 
	
	@GetMapping("/compras/page/{page}")
	public Page<Compra> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 4);
		return facturaService.findAll(pageable);
	}
	//REPORTES
		@GetMapping("/compras/findAllFechas/{fecha1}/{fecha2}")
		public HttpServletResponse indexFechas(HttpServletResponse response, @PathVariable String fecha1, @PathVariable String fecha2) throws Exception {
			//return facturaService.findAllFechas(fecha1,fecha2);
			
			//List<Factura>
			
			response.setContentType("application/octet-stream");
	        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	        String currentDateTime = dateFormatter.format(new Date());
	         
	        String headerKey = "Content-Disposition";
	        String headerValue = "attachment; filename=Compras_" + fecha1 + "_Hasta_" +fecha2 + ".xlsx";
	        response.setHeader(headerKey, headerValue);
	         
	        List<Factura> listaExcel = new ArrayList<>();
	        try {
	            List<Compra> datos = facturaService.findAllFechas(fecha1,fecha2);      
	       /*  Compra comp= new Compra();
	         Proveedor proveedor= new Proveedor();
	         proveedor.setId(new Long(0));
	         proveedor.setCedula("_");
	         proveedor.setNombre("_");
	         proveedor.setCedula("_");
    		 comp.setId(new Long(0));
    		  LocalDateTime fecha=LocalDateTime.now();
    		 comp.setProveedor(proveedor);
    		 comp.setCreateAt(fecha);
    		 comp.setObservacion("_");
    		 comp.setDescripcion("_");
    		 
    		 comp.setTotal(total);*/
	            Double total = facturaService.getTotalFechas(fecha1, fecha2);
    		 //datos.add(comp);
	            ComprasExcelDto excelExporter = new ComprasExcelDto(datos,total);            
	            excelExporter.export(response);   
	          
	          
	        } catch (Exception e) {
		            throw new Exception( "No se pudo obtener el listado de aportes",e.getCause());
			}
			return null;   
		
			
		}
		
}
