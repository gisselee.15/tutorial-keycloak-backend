package com.tutorial.keycloakbackend.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tutorial.keycloakbackend.models.entity.Entrada;
import com.tutorial.keycloakbackend.models.entity.ItemEntrada;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.service.IClienteService;
import com.tutorial.keycloakbackend.models.service.IEntradaService;
import com.tutorial.keycloakbackend.models.service.IProductoService;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
@ComponentScan("com.tutorial.keycloakbackend.controller")
public class EntradaRestController {
	@Autowired
	private IClienteService clienteService;
	@Autowired
	private IEntradaService entradaService;

	
	@Autowired
	private IProductoService productoService;
	@GetMapping("/entradas/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Entrada show(@PathVariable Long id) {
		return entradaService.findById(id);
	}
	
	@DeleteMapping("/entradas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		entradaService.delete(id);
	}
	

	@GetMapping("/entradas/filtrar-productos/{term}")
	@ResponseStatus(HttpStatus.OK)
	public List<Producto> filtrarProductos(@PathVariable String term){
		return clienteService.findProductoByNombre(term);
	}
	
	@PostMapping("/entradas")
	@ResponseStatus(HttpStatus.CREATED)
	public Entrada crear(@RequestBody Entrada entrada) {
		try {
			//agregar stok productos
			if(entrada.getItems().size()>0) {
				for(ItemEntrada item : entrada.getItems()) {
					
					Producto clienteActual = productoService.findById(item.getProducto().getId());
					if (clienteActual != null  && entrada.getDescripcion().equalsIgnoreCase("entrada")) {
						clienteActual.setExistente(clienteActual.getExistente()-item.getCantidad());
						productoService.save(clienteActual);
					}else {
						clienteActual.setExistente(clienteActual.getExistente()+item.getCantidad());
						productoService.save(clienteActual);
					}
				}
			}
			return entradaService.save(entrada);
		}catch(Exception e) {
			return null;
		}
	}
	
	@GetMapping("/entradas")
	public List<Entrada> index() {
		return entradaService.findAll();
	} 
	
	@GetMapping("/entradas/page/{page}")
	public Page<Entrada> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return entradaService.findAll(pageable);
	}
	
	
}
