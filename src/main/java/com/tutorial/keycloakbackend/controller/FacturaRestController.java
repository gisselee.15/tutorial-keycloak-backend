package com.tutorial.keycloakbackend.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.spire.xls.FileFormat;
import com.spire.xls.Workbook;
import com.tutorial.keycloakbackend.dto.DonwloadAnualPDFReport;
import com.tutorial.keycloakbackend.dto.FilaTotalFinalVentas;
import com.tutorial.keycloakbackend.dto.VentasExcelDto;
import com.tutorial.keycloakbackend.models.entity.Cliente;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.ItemFactura;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.service.IClienteService;
import com.tutorial.keycloakbackend.models.service.IFacturaService;
import com.tutorial.keycloakbackend.models.service.IProductoService;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/api")
@ComponentScan("com.tutorial.keycloakbackend.controller")
public class FacturaRestController {
	private DonwloadAnualPDFReport don;

	@Autowired
	private IFacturaService facturaService;

	@Autowired
	private IClienteService clienteService;
	@Autowired
	private IProductoService productoService;
	@GetMapping("/facturas/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Factura show(@PathVariable Long id) {
		return clienteService.findFacturaById(id);
	}
	
	@DeleteMapping("/facturas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		clienteService.deleteFacturaById(id);
	}
	
	@GetMapping("/facturas/filtrar-productos/{term}")
	@ResponseStatus(HttpStatus.OK)
	public List<Producto> filtrarProductos(@PathVariable String term){
		return clienteService.findProductoByNombre(term);
	}
	
	@PostMapping("/facturas")
	@ResponseStatus(HttpStatus.CREATED)
	public Factura crear(@RequestBody Factura factura) {
		try {
			//agregar stok productos
			if(factura.getItems().size()>0) {
				for(ItemFactura item : factura.getItems()) {
					
					Producto clienteActual = productoService.findById(item.getProducto().getId());
			
					Producto clienteUpdated = null;
					if (clienteActual != null  && clienteActual.getTipo().equalsIgnoreCase("almacen")) {
						clienteActual.setNombre(clienteActual.getNombre());
						clienteActual.setPrecio(clienteActual.getPrecio());
						clienteActual.setCreateAt(clienteActual.getCreateAt());
						clienteActual.setTipo(clienteActual.getTipo());
						clienteActual.setExistente(clienteActual.getExistente()-item.getCantidad());
						clienteActual.setMedida(clienteActual.getMedida());
						clienteUpdated = productoService.save(clienteActual);
					}
					if (clienteActual.getTipo().equalsIgnoreCase("preparados")) {
						item.setEstado("pendiente");
					}
				}
			}
			return clienteService.saveFactura(factura);
		}catch(Exception e) {
			return null;
		}
	}
	@GetMapping("/facturas")
	public List<Factura> index() {
		return facturaService.findAll();
	} 
	
	@GetMapping("/facturas/page/{page}")
	public Page<Factura> index(@PathVariable Integer page) {
		Pageable pageable = PageRequest.of(page, 10);
		return facturaService.findAll(pageable);
	}
	//REPORTES
	@GetMapping("/facturas/findAllFechas/{fecha1}/{fecha2}")
	public HttpServletResponse indexFechas(HttpServletResponse response, @PathVariable String fecha1, @PathVariable String fecha2) throws Exception {
		//return facturaService.findAllFechas(fecha1,fecha2);
		
		//List<Factura>
		
		response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String currentDateTime = dateFormatter.format(new Date());
         
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=Ventas_" + fecha1 + "_Hasta_" +fecha2 + ".xlsx";
        response.setHeader(headerKey, headerValue);
         
        List<Factura> listaExcel = new ArrayList<>();
        try {
            List<Factura> datos = facturaService.findAllFechas(fecha1,fecha2);      
            /*Factura aedto = new Factura();
            Cliente cli= new Cliente();
            cli.setNombre("_");
            cli.setCedula("_");
            LocalDateTime fecha=LocalDateTime.now();
            aedto.setId(new Long(0));
            aedto.setCliente(cli);
            aedto.setDescripcion("_");
            aedto.setObservacion(" Monto Total");
            aedto.setCreateAt(fecha);
            aedto.setUsuario("_");*/
            Double total = facturaService.getTotalFechas(fecha1, fecha2);
            //aedto.setTotal(total);
            //agregar el del total de cajadao
            /*for (Factura ap : datos) {           	
            		 
            	Factura aedto = new Factura();
            	aedto.setId(ap.getId());
            	aedto.setFecha(ap.getFecha() != null ? ap.getFecha().toString() : null );
            	aedto.setImporte(ap.getImporte());
            	aedto.setNroOperacion(ap.getNroOperacion());
            	aedto.setAnho(ap.getAnho() != null ? ap.getAnho().intValue() : null);
            	aedto.setMes(ap.getMes() != null ? ap.getMes().intValue(): null);
            	aedto.setCuentaDestino(ap.getCuentaDestino());
            	aedto.setCuentaOrigen(ap.getCuentaOrigen());
            	aedto.setEstado(ap.getEstado() != null ? ap.getEstado() : "");
            	aedto.setObservacion(ap.getObservacion() != null ? ap.getObservacion() : "");
            	aedto.setRazonSocial(ap.getPatronal().getRazonSocial());        	
            	
            	listaExcel.add(ap);
            	
    		} */
           // datos.add(aedto);
            VentasExcelDto excelExporter = new VentasExcelDto(datos, total);            
            excelExporter.export(response);   
          
          
        } catch (Exception e) {
	            throw new Exception( "No se pudo obtener el listado de aportes",e.getCause());
		}
		return null;   
	
		
	}
	
	

}
