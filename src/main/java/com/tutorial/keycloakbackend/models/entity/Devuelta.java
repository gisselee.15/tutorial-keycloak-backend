package com.tutorial.keycloakbackend.models.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "devueltas")
public class Devuelta implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "DevueltasSeq")
	@SequenceGenerator(name="DevueltasSeq",sequenceName="devueltas_seq", allocationSize=1)
	private Long id;

	private String descripcion;

	private String observacion;

	@Column(name = "create_at")
	private LocalDateTime createAt;//@Temporal(TemporalType.DATE)

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "devuelta_id")
	private List<ItemDevuelta> items;
	
	private Double total;
	private String usuario;

	public void setTotal(Double total) {
		this.total = total;
	}

	public Devuelta() {
		items = new ArrayList<>();
	}

	@PrePersist
	public void prePersist() throws ParseException {
		LocalDateTime ahora = LocalDateTime.now();
	        String timeZoneId = "America/Asuncion";
	        TimeZone timeZone = TimeZone.getTimeZone(timeZoneId);
	        this.createAt=ahora;
	    
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	


	public LocalDateTime getCreateAt() {
		return createAt;
	}

	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}



	public List<ItemDevuelta> getItems() {
		return items;
	}

	public void setItems(List<ItemDevuelta> items) {
		for (ItemDevuelta item : items) {
			item.setFactura(this);
		}
		this.items = items;
	}

	public Double getTotal() {
		Double total = 0.00;
		for (ItemDevuelta item : items) {
			total += item.getImporte();
		}
		return total;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	private static final long serialVersionUID = 1L;
}
