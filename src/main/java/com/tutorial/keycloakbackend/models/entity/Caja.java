package com.tutorial.keycloakbackend.models.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "caja")
public class Caja implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CajaSeq")
	@SequenceGenerator(name="CajaSeq",sequenceName="caja_seq", allocationSize=1)
	private Long id;


	private String usuario;

	private Double montoinicial;
	private Double montofinal;
	private Double ventastotal;
	private String estado;
	@Column(name = "inicial_at")
	private LocalDateTime inicialAt;
	
	
	@Column(name = "final_at")
	private LocalDateTime finalAt;

	@PrePersist
	public void prePersist() {
		LocalDateTime ahora = LocalDateTime.now();
		this.finalAt = ahora;
		this.inicialAt=ahora;
	}

	




	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getUsuario() {
		return usuario;
	}



	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}



	public Double getMontoinicial() {
		return montoinicial;
	}



	public void setMontoinicial(Double montoinicial) {
		this.montoinicial = montoinicial;
	}



	public Double getMontofinal() {
		return montofinal;
	}



	public void setMontofinal(Double montofinal) {
		this.montofinal = montofinal;
	}



	public Double getVentastotal() {
		return ventastotal;
	}



	public void setVentastotal(Double ventastotal) {
		this.ventastotal = ventastotal;
	}


	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}



	public LocalDateTime getInicialAt() {
		return inicialAt;
	}






	public void setInicialAt(LocalDateTime inicialAt) {
		this.inicialAt = inicialAt;
	}






	public LocalDateTime getFinalAt() {
		return finalAt;
	}






	public void setFinalAt(LocalDateTime finalAt) {
		this.finalAt = finalAt;
	}



	private static final long serialVersionUID = 1L;
}
