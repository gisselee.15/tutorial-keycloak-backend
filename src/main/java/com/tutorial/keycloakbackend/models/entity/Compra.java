package com.tutorial.keycloakbackend.models.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "compras")
public class Compra implements Serializable {

	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ComprasSeq")
	@SequenceGenerator(name="ComprasSeq",sequenceName="compras_seq", allocationSize=1)
	private Long id;

	private String descripcion;

	private String observacion;

	@Column(name = "create_at")
	private LocalDateTime createAt;//@Temporal(TemporalType.DATE)

	@JsonIgnoreProperties(value={"compras", "hibernateLazyInitializer", "handler"}, allowSetters=true)
	@ManyToOne(fetch = FetchType.LAZY)
	private Proveedor proveedor;

	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "compra_id")
	private List<ItemCompra> items;
	private Double total;
	
	public void setTotal(Double total) {
		this.total = total;
	}

	public Compra() {
		items = new ArrayList<>();
	}

	@PrePersist
	public void prePersist() {
		LocalDateTime ahora = LocalDateTime.now();
        this.createAt=ahora;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	
	
	public LocalDateTime getCreateAt() {
		return createAt;
	}

	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}

	public List<ItemCompra> getItems() {
		return items;
	}

	public void setItems(List<ItemCompra> items) {
		for (ItemCompra item : items) {
			item.setCompra(this);
		}
		this.items = items;
	}

	public Double getTotal() {
		Double total = 0.00;
		for (ItemCompra item : items) {
			total += item.getImporte();
		}
		return total;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	private static final long serialVersionUID = 1L;
}
