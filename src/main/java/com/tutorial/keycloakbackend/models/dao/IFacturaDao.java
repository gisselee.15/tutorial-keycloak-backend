package com.tutorial.keycloakbackend.models.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.ItemFactura;
import com.tutorial.keycloakbackend.models.entity.Producto;

@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IFacturaDao extends CrudRepository<Factura, Long>{
	public Page<Factura> findAll(Pageable pageable);
	
	@Query("select f from Factura f where f.createAt between ?1 and ?2")
	public List<Factura> findAllFechas( LocalDateTime term1,  LocalDateTime term2);

	@Query("select  sum(f.total) from Factura f where  f.createAt between ?1 and ?2")
	public  Double getTotalFechas( LocalDateTime term1,  LocalDateTime term2);
	
}
