package com.tutorial.keycloakbackend.models.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.tutorial.keycloakbackend.models.entity.Caja;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.Producto;

@ComponentScan("com.tutorial.keycloakbackend.models")
public interface CajaDao extends CrudRepository<Caja, Long>{
	@Query("select c from Caja c where c.usuario like %?1% and c.estado like 'abierto' ")
	public List<Caja> findByUsuario(String term);
	//public List<Producto> findByNombreContainingIgnoreCase(String term);
	
	//public List<Producto> findByNombreStartingWithIgnoreCase(String term);
	public Page<Caja> findAll(Pageable pageable);
	@Query("select  sum(f.total) from Factura f where f.usuario like %?1% and f.createAt  >=  ?2")
	public  Double findVentasByUsuario(String term, LocalDateTime term2 );
	
}
