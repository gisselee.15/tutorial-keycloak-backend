package com.tutorial.keycloakbackend.models.dao;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.tutorial.keycloakbackend.models.entity.Producto;

@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IProductoDao extends CrudRepository<Producto, Long>{
	@Query("select p from Producto p where p.nombre like %?1%")
	public List<Producto> findByNombre(String term);
	
	public List<Producto> findByNombreContainingIgnoreCase(String term);
	
	public List<Producto> findByNombreStartingWithIgnoreCase(String term);

	public Page<Producto> findAll(Pageable pageable);
}
