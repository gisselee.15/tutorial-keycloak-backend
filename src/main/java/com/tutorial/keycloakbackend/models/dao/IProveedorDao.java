package com.tutorial.keycloakbackend.models.dao;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tutorial.keycloakbackend.models.entity.Cliente;
import com.tutorial.keycloakbackend.models.entity.Proveedor;
import com.tutorial.keycloakbackend.models.entity.Region;

@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IProveedorDao extends JpaRepository<Proveedor, Long>{

	@Query("from Region")
	public List<Region> findAllRegiones();
}
