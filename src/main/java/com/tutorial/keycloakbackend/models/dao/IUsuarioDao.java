package com.tutorial.keycloakbackend.models.dao;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.tutorial.keycloakbackend.models.entity.Cliente;
import com.tutorial.keycloakbackend.models.entity.Region;
import com.tutorial.keycloakbackend.models.entity.Usuario;

@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IUsuarioDao extends JpaRepository<Usuario, Long>{

	@Query("from Region")
	public List<Region> findAllRegiones();
}
