package com.tutorial.keycloakbackend.models.dao;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.tutorial.keycloakbackend.models.entity.Entrada;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.ItemFactura;
import com.tutorial.keycloakbackend.models.entity.Producto;

@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IEntradaDao extends CrudRepository<Entrada, Long>{
	public Page<Entrada> findAll(Pageable pageable);
	
}
