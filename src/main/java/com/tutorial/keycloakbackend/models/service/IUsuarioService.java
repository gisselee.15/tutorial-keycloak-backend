package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Usuario;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.entity.Region;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IUsuarioService {

	public List<Usuario> findAll();
	
	public Page<Usuario> findAll(Pageable pageable);
	
	public Usuario findById(Long id);
	
	public Usuario save(Usuario Usuario);
	
	public void delete(Long id);
	
	public List<Region> findAllRegiones();
	
	

}
