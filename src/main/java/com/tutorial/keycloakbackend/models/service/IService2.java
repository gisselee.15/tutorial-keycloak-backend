package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Factura;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IService2 {

public List<Factura> findAll();
	
	public Page<Factura> findAll(Pageable pageable);
	
	public Factura findById(Long id);
	
	public Factura save(Factura rol);
	
	public void delete(Long id);

}
