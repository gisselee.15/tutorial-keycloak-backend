package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Cliente;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.entity.Region;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IClienteService {

	public List<Cliente> findAll();
	
	public Page<Cliente> findAll(Pageable pageable);
	
	public Cliente findById(Long id);
	
	public Cliente save(Cliente cliente);
	
	public void delete(Long id);
	
	public List<Region> findAllRegiones();
	
	public Factura findFacturaById(Long id);
	
	public Factura saveFactura(Factura factura);
	
	public void deleteFacturaById(Long id);
	
	public List<Producto> findProductoByNombre(String term);

}
