package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.models.dao.IProveedorDao;
import com.tutorial.keycloakbackend.models.dao.ICompraDao;
import com.tutorial.keycloakbackend.models.dao.IProductoDao;
import com.tutorial.keycloakbackend.models.entity.Proveedor;
import com.tutorial.keycloakbackend.models.entity.Compra;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.entity.Region;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class ProveedorServiceImpl implements IProveedorService {
	@Autowired
	private IProveedorDao proveedorDao;
	
	@Autowired
	private ICompraDao comprasDao;
	
	@Autowired
	private IProductoDao productoDao;

	@Override
	@Transactional(readOnly = true)
	public List<Proveedor> findAll() {
		return (List<Proveedor>) proveedorDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Proveedor> findAll(Pageable pageable) {
		return proveedorDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Proveedor findById(Long id) {
		return proveedorDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Proveedor save(Proveedor proveedor) {
		return proveedorDao.save(proveedor);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		proveedorDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Region> findAllRegiones() {
		return proveedorDao.findAllRegiones();
	}

	@Override
	@Transactional(readOnly = true)
	public Compra findCompraById(Long id) {
		return comprasDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Compra saveCompra(Compra compra) {
		return comprasDao.save(compra);
	}

	@Override
	@Transactional
	public void deleteCompraById(Long id) {
		comprasDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Producto> findProductoByNombre(String term) {
		return productoDao.findByNombreContainingIgnoreCase(term);
	}

}
