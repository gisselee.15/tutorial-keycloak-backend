package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.models.dao.IEntradaDao;
import com.tutorial.keycloakbackend.models.entity.Entrada;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class EntradaServiceImpl implements IEntradaService {
	@Autowired
	private IEntradaDao rolDao;
	
	@Override	
	@Transactional(readOnly = true)
	public List<Entrada> findAll() {
		return (List<Entrada>) rolDao.findAll();
	}
	@Override
	@Transactional(readOnly = true)
	public Page<Entrada> findAll(Pageable pageable) {
		return rolDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Entrada findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Entrada save(Entrada rol) {
		return rolDao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		rolDao.deleteById(id);
		
	}
	/*@Autowired
	private IEntradaDao EntradaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Entrada> findAll() {
		return (List<Entrada>) EntradaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Entrada> findAll(Pageable pageable) {
		return EntradaDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Entrada findById(Long id) {
		return EntradaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Entrada save(Entrada Entrada) {
		return EntradaDao.save(Entrada);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		EntradaDao.deleteById(id);
	}*/


}
