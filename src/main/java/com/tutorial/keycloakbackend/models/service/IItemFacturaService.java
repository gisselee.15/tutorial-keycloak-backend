package com.tutorial.keycloakbackend.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Cliente;
import com.tutorial.keycloakbackend.models.entity.ItemFactura;
import com.tutorial.keycloakbackend.models.entity.ItemsFacturas;
import com.tutorial.keycloakbackend.models.entity.Producto;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IItemFacturaService {

public List<ItemsFacturas> findAll();
	
	public Page<ItemsFacturas> findAll(Pageable pageable);
	public ItemsFacturas findById(Long id);


	public ItemsFacturas save(ItemsFacturas cliente);


}
