
package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Producto;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IProductoService {
public List<Producto> findAll();
	
	public Page<Producto> findAll(Pageable pageable);
	
	public Producto findById(Long id);
	
	public Producto save(Producto rol);
	
	public void delete(Long id);
}
