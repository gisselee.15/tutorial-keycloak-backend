package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Entrada;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IEntradaService {

public List<Entrada> findAll();
	
	public Page<Entrada> findAll(Pageable pageable);
	
	public Entrada findById(Long id);
	
	public Entrada save(Entrada rol);
	
	public void delete(Long id);

}
