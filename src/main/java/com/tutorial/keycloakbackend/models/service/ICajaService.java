
package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Caja;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.Producto;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface ICajaService {
	public List<Caja> findAll();
	
	public Page<Caja> findAll(Pageable pageable);
	
	public Caja findById(Long id);
	public  List<Caja>  findByUsuario(String username);
	public   Double findVentasByUsuario(String username, String fecha);

	public Caja save(Caja rol);
	
	public void delete(Long id);
	
}
