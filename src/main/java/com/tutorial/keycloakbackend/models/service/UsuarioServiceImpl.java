package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.models.dao.IUsuarioDao;
import com.tutorial.keycloakbackend.models.dao.IFacturaDao;
import com.tutorial.keycloakbackend.models.dao.IProductoDao;
import com.tutorial.keycloakbackend.models.dao.IUsuarioDao;
import com.tutorial.keycloakbackend.models.entity.Usuario;
import com.tutorial.keycloakbackend.models.entity.Factura;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.entity.Region;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class UsuarioServiceImpl implements IUsuarioService {
	@Autowired
	private IUsuarioDao UsuarioDao;
	


	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return (List<Usuario>) UsuarioDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Usuario> findAll(Pageable pageable) {
		return UsuarioDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Usuario findById(Long id) {
		return UsuarioDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Usuario save(Usuario Usuario) {
		return UsuarioDao.save(Usuario);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		UsuarioDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Region> findAllRegiones() {
		return UsuarioDao.findAllRegiones();
	}

	
}
