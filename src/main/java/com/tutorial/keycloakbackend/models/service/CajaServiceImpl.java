package com.tutorial.keycloakbackend.models.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.models.dao.CajaDao;
import com.tutorial.keycloakbackend.models.entity.Caja;
import com.tutorial.keycloakbackend.models.entity.Factura;

@Service

@ComponentScan("com.tutorial.keycloakbackend.models")
public class CajaServiceImpl implements ICajaService {
	@Autowired
	private CajaDao rolDao;
	
	@Override	
	@Transactional(readOnly = true)
	public List<Caja> findAll() {
		return (List<Caja>) rolDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Caja> findAll(Pageable pageable) {
		return rolDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Caja findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Caja save(Caja rol) {
		return rolDao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		rolDao.deleteById(id);
		
	}
	@Override
	public  List<Caja>  findByUsuario(String username) {
		// TODO Auto-generated method stub
		return rolDao.findByUsuario(username);
	}

	@Override
	public Double findVentasByUsuario(String username, String fecha) {
		Double result=(double) 0;
		LocalDateTime dateTime = LocalDateTime.parse(fecha);
		//Date var = Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
		//2021-07-01T13:26:46.261
		fecha=fecha.replace("T", " ");
		fecha=fecha.substring(0,19);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
		LocalDateTime var = LocalDateTime.parse(fecha,formatter);
	result=rolDao.findVentasByUsuario(username,var);
		
		 return result;
	}

	/*@Autowired
	private IClienteDao clienteDao;

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() {
		return (List<Cliente>) clienteDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Cliente> findAll(Pageable pageable) {
		return clienteDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Cliente findById(Long id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Cliente save(Cliente cliente) {
		return clienteDao.save(cliente);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		clienteDao.deleteById(id);
	}*/

	
}
