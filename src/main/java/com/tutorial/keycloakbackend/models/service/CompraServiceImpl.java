package com.tutorial.keycloakbackend.models.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.models.dao.ICompraDao;
import com.tutorial.keycloakbackend.models.entity.Compra;
import com.tutorial.keycloakbackend.models.entity.Factura;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class CompraServiceImpl implements ICompraService {
	@Autowired
	private ICompraDao rolDao;
	
	@Override	
	@Transactional(readOnly = true)
	public List<Compra> findAll() {
		return (List<Compra>) rolDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Compra> findAll(Pageable pageable) {
		return rolDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Compra findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Compra save(Compra rol) {
		return rolDao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		rolDao.deleteById(id);
		
	}
	/*@Autowired
	private ICompraDao CompraDao;

	@Override
	@Transactional(readOnly = true)
	public List<Compra> findAll() {
		return (List<Compra>) CompraDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Compra> findAll(Pageable pageable) {
		return CompraDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Compra findById(Long id) {
		return CompraDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Compra save(Compra Compra) {
		return CompraDao.save(Compra);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		CompraDao.deleteById(id);
	}*/

	@Override
	public List<Compra> findAllFechas(String fecha1,String fecha2) {
		fecha1=fecha1+(" 00:00:00");
		fecha2=fecha2+(" 00:00:00");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
		LocalDateTime term1 = LocalDateTime.parse(fecha1,formatter);
		LocalDateTime term2 = LocalDateTime.parse(fecha2,formatter);
		return rolDao.findAllFechas(term1, term2);
	}
	@Override
	public Double getTotalFechas(String fecha1, String fecha2) {
		Double result;
		fecha1=fecha1+(" 00:00:00");
		fecha2=fecha2+(" 00:00:00");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
		LocalDateTime term1 = LocalDateTime.parse(fecha1,formatter);
		LocalDateTime term2 = LocalDateTime.parse(fecha2,formatter);
		result=rolDao.getTotalFechas(term1,term2);
		
		 return result;
	}

}
