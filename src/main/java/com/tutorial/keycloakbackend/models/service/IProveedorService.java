package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Proveedor;
import com.tutorial.keycloakbackend.models.entity.Compra;
import com.tutorial.keycloakbackend.models.entity.Producto;
import com.tutorial.keycloakbackend.models.entity.Region;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IProveedorService {

	public List<Proveedor> findAll();
	
	public Page<Proveedor> findAll(Pageable pageable);
	
	public Proveedor findById(Long id);
	
	public Proveedor save(Proveedor cliente);
	
	public void delete(Long id);
	
	public List<Region> findAllRegiones();
	
	public Compra findCompraById(Long id);
	
	public Compra saveCompra(Compra compra);
	
	public void deleteCompraById(Long id);
	
	public List<Producto> findProductoByNombre(String term);

}
