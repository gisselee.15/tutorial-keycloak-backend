package com.tutorial.keycloakbackend.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.dto.ItemsFacturasDetalle;
import com.tutorial.keycloakbackend.models.dao.IItemFacturaDao;
import com.tutorial.keycloakbackend.models.entity.ItemFactura;
import com.tutorial.keycloakbackend.models.entity.ItemsFacturas;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class ItemFacturaServiceImpl implements IItemFacturaService {
	@Autowired
	private IItemFacturaDao rolDao;
	
	@Override	
	@Transactional(readOnly = true)
	public List<ItemsFacturas> findAll() {
		return (List<ItemsFacturas>) rolDao.findAll();
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public Page<ItemsFacturas> findAll(Pageable pageable) {
		ItemsFacturasDetalle itemsFacturasDetalle= new ItemsFacturasDetalle();		
		return rolDao.findAll(pageable);
	}


	@Override
	public ItemsFacturas findById(Long id) {
		// 		return rolDao.findById(id).orElse(null);

		return rolDao.findById(id).orElse(null);
	}


	@Override
	public ItemsFacturas save(ItemsFacturas cliente) {
		// TODO Auto-generated method stub
		return rolDao.save(cliente);
	}



	/*@Override
	@Transactional(readOnly = true)
	public ItemFactura findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ItemFactura save(ItemFactura rol) {
		return rolDao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		rolDao.deleteById(id);
		
	}*/
	/*@Autowired
	private IItemFacturaDao ItemFacturaDao;

	@Override
	@Transactional(readOnly = true)
	public List<ItemFactura> findAll() {
		return (List<ItemFactura>) ItemFacturaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ItemFactura> findAll(Pageable pageable) {
		return ItemFacturaDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public ItemFactura findById(Long id) {
		return ItemFacturaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ItemFactura save(ItemFactura ItemFactura) {
		return ItemFacturaDao.save(ItemFactura);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ItemFacturaDao.deleteById(id);
	}*/

	


}
