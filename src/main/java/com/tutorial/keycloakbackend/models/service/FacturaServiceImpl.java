package com.tutorial.keycloakbackend.models.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.models.dao.IFacturaDao;
import com.tutorial.keycloakbackend.models.entity.Factura;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class FacturaServiceImpl implements IFacturaService {
	@Autowired
	private IFacturaDao rolDao;
	
	@Override	
	@Transactional(readOnly = true)
	public List<Factura> findAll() {
		return (List<Factura>) rolDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Factura> findAll(Pageable pageable) {
		return rolDao.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public Factura findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Factura save(Factura rol) {
		return rolDao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		rolDao.deleteById(id);
		
	}
	/*@Autowired
	private IFacturaDao FacturaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Factura> findAll() {
		return (List<Factura>) FacturaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Factura> findAll(Pageable pageable) {
		return FacturaDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public Factura findById(Long id) {
		return FacturaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Factura save(Factura Factura) {
		return FacturaDao.save(Factura);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		FacturaDao.deleteById(id);
	}*/

	@Override
	public List<Factura> findAllFechas(String fecha1,String fecha2) {
		fecha1=fecha1+(" 00:00:00");
		fecha2=fecha2+(" 00:00:00");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
		LocalDateTime term1 = LocalDateTime.parse(fecha1,formatter);
		LocalDateTime term2 = LocalDateTime.parse(fecha2,formatter);
		return rolDao.findAllFechas(term1, term2);
	}

	@Override
	public Double getTotalFechas(String fecha1, String fecha2) {
		Double result=(double) 0;
		fecha1=fecha1+(" 00:00:00");
		fecha2=fecha2+(" 00:00:00");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"); 
		LocalDateTime term1 = LocalDateTime.parse(fecha1,formatter);
		LocalDateTime term2 = LocalDateTime.parse(fecha2,formatter);
		result=rolDao.getTotalFechas(term1,term2);
		
		 return result;
	}


}
