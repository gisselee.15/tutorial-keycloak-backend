package com.tutorial.keycloakbackend.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.ItemsCompras;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface IItemCompraService {

public List<ItemsCompras> findAll();
	
	public Page<ItemsCompras> findAll(Pageable pageable);
	public ItemsCompras findById(Long id);


	public ItemsCompras save(ItemsCompras cliente);


}
