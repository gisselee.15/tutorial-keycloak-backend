package com.tutorial.keycloakbackend.models.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tutorial.keycloakbackend.dto.ItemsComprasDetalle;
import com.tutorial.keycloakbackend.models.dao.IItemCompraDao;
import com.tutorial.keycloakbackend.models.dao.IItemFacturaDao;
import com.tutorial.keycloakbackend.models.entity.ItemFactura;
import com.tutorial.keycloakbackend.models.entity.ItemsCompras;

@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public class ItemCompraServiceImpl implements IItemCompraService {
	@Autowired
	private IItemCompraDao rolDao;
	
	@Override	
	@Transactional(readOnly = true)
	public List<ItemsCompras> findAll() {
		return (List<ItemsCompras>) rolDao.findAll();
	}
	
	
	@Override
	@Transactional(readOnly = true)
	public Page<ItemsCompras> findAll(Pageable pageable) {
		ItemsComprasDetalle itemsFacturasDetalle= new ItemsComprasDetalle();		
		return rolDao.findAll(pageable);
	}


	@Override
	public ItemsCompras findById(Long id) {
		// 		return rolDao.findById(id).orElse(null);

		return rolDao.findById(id).orElse(null);
	}


	@Override
	public ItemsCompras save(ItemsCompras cliente) {
		// TODO Auto-generated method stub
		return rolDao.save(cliente);
	}



	/*@Override
	@Transactional(readOnly = true)
	public ItemFactura findById(Long id) {
		return rolDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ItemFactura save(ItemFactura rol) {
		return rolDao.save(rol);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		rolDao.deleteById(id);
		
	}*/
	/*@Autowired
	private IItemFacturaDao ItemFacturaDao;

	@Override
	@Transactional(readOnly = true)
	public List<ItemFactura> findAll() {
		return (List<ItemFactura>) ItemFacturaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Page<ItemFactura> findAll(Pageable pageable) {
		return ItemFacturaDao.findAll(pageable);
	}
	
	@Override
	@Transactional(readOnly = true)
	public ItemFactura findById(Long id) {
		return ItemFacturaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public ItemFactura save(ItemFactura ItemFactura) {
		return ItemFacturaDao.save(ItemFactura);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		ItemFacturaDao.deleteById(id);
	}*/

	


}
