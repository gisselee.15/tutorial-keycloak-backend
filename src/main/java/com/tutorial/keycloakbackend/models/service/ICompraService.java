package com.tutorial.keycloakbackend.models.service;

import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tutorial.keycloakbackend.models.entity.Compra;
import com.tutorial.keycloakbackend.models.entity.Factura;
@Service
@ComponentScan("com.tutorial.keycloakbackend.models")
public interface ICompraService {

public List<Compra> findAll();
	
	public Page<Compra> findAll(Pageable pageable);
	
	public Compra findById(Long id);
	
	public Compra save(Compra rol);
	
	public void delete(Long id);

	public List<Compra> findAllFechas(String fecha1, String fecha2);
	public Double getTotalFechas(String fecha1,String fecha2);

}
